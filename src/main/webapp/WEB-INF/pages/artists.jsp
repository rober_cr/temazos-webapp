<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Artists</title>
</head>
<body>
	<h1>Artistazos</h1>
	<c:if test="${not empty artists}">
		<ul>
			<c:forEach var="artist" items="${artists}">
				<li>
					<a href="artists/${artist.id}">
						<c:out value="${artist.name}" />
					</a>
				</li>
			</c:forEach>
		</ul>
	</c:if>
</body>
</html>