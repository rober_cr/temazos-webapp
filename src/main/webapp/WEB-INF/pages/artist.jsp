<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${artist.name}</title>
	<style type="text/css">
		img {
			width: 150px;
		}
	</style>
</head>
<body>
	<h1>${artist.name }</h1>
	<img id="photo" src="${artist.photoUrl }" />
	<br />
	<a href="/artists">volver</a>
</body>
</html>