package test.hotelbeds.temazos.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * The Class Artist.
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "create")
@Entity
public class Artist implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long	serialVersionUID	= 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long				id;

	/** The name. */
	@NonNull
	private String				name;

	/** The photo url. */
	@NonNull
	private String				photoUrl;

}
