package test.hotelbeds.temazos.controller;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import test.hotelbeds.temazos.model.Artist;
import test.hotelbeds.temazos.service.ArtistService;

/**
 * The Class ArtistController.
 */
@RestController()
@EnableAutoConfiguration
@RequestMapping(value = "/API/artists")
@Setter
public class ArtistAPIController {

	/** The artist service. */
	@Autowired
	private ArtistService	artistService;

	/**
	 * Creates the.
	 *
	 * @param artist
	 *            the artist
	 * @return the artist
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Artist create(@RequestBody Artist artist) {
		return artistService.save(artist);
	}

	/**
	 * Delete.
	 *
	 * @param idParam
	 *            the id param
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable(value = "id") final String idParam) {
		Long id = Long.valueOf(idParam);
		artistService.delete(id);
	}

	/**
	 * Find artist.
	 *
	 * @param idParam
	 *            the id param
	 * @return the artist
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Artist findArtist(@PathVariable(value = "id") final String idParam) {
		Long id = Long.valueOf(idParam);
		return artistService.findOne(id);
	}

	/**
	 * Find artists.
	 *
	 * @return the iterable
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Artist> findArtists() {
		return artistService.findAll();
	}

	/**
	 * Update.
	 *
	 * @param id
	 *            the id
	 * @param artist
	 *            the artist
	 * @return the artist
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody Artist update(@PathVariable(value = "id") final String id, @RequestBody Artist artist) {
		return artistService.save(artist);
	}
}
