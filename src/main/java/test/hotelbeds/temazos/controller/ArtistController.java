package test.hotelbeds.temazos.controller;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import test.hotelbeds.temazos.model.Artist;
import test.hotelbeds.temazos.service.ArtistService;

/**
 * The Class ArtistController.
 */
@Controller
@EnableAutoConfiguration
@RequestMapping(value = "/artists")
@Setter
public class ArtistController {

	/** The artist service. */
	@Autowired
	private ArtistService	artistService;

	/**
	 * Find artist.
	 *
	 * @param idParam
	 *            the id param
	 * @return the artist
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView findArtist(@PathVariable(value = "id") final String idParam) {
		Long id = Long.valueOf(idParam);
		Artist artist = artistService.findOne(id);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("artist");
		mav.addObject("artist", artist);
		return mav;
	}

	/**
	 * Find artists.
	 *
	 * @return the iterable
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView findArtists() {
		Iterable<Artist> artists = artistService.findAll();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("artists");
		mav.addObject("artists", artists);
		return mav;
	}
}