package test.hotelbeds.temazos.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import test.hotelbeds.temazos.model.Artist;

/**
 * The Interface ArtistRepository.
 */
@Repository
public interface ArtistRepository extends PagingAndSortingRepository<Artist, Long> {

}
