package test.hotelbeds.temazos;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import test.hotelbeds.temazos.model.Artist;
import test.hotelbeds.temazos.service.ArtistService;

/**
 * The Class TemazosWebappApplication.
 */
@SpringBootApplication
@EnableWebMvc
public class TemazosWebappApplication extends WebMvcConfigurerAdapter {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(TemazosWebappApplication.class, args);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureDefaultServletHandling(org.springframework.web.servlet.config.annotation.
	 * DefaultServletHandlerConfigurer)
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	/**
	 * Initialitzer.
	 *
	 * @param artistService
	 *            the artist service
	 * @return the command line runner
	 */
	@Bean
	@Profile("PROD")
	public CommandLineRunner initialitzer(ArtistService artistService) {
		return (args) -> {
			artistService.save(Artist.create("Chimo Bayo", "http://www.kulturaldia.com/wp-web/wp-content/uploads/2015/07/chimo-as%C3%AD-me-gusta-a-m%C3%AD.jpg"));
			artistService.save(Artist.create("Paco Pil", "http://www.wikirevistes.org/wiki/images/thumb/5/5d/Paco_pil250px.png/180px-Paco_pil250px.png"));
		};
	}

	/**
	 * View resolver.
	 *
	 * @return the internal resource view resolver
	 */
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/pages/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

}
