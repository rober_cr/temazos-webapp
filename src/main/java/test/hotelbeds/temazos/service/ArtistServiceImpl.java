package test.hotelbeds.temazos.service;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.hotelbeds.temazos.model.Artist;
import test.hotelbeds.temazos.repository.ArtistRepository;

/**
 * The Class ArtistServiceImpl.
 */
@Service
/**
 * Sets the artist repository.
 *
 * @param artistRepository the new artist repository
 */
@Setter
public class ArtistServiceImpl implements ArtistService {

	/** The artist repository. */
	@Autowired
	private ArtistRepository	artistRepository;

	/*
	 * (non-Javadoc)
	 *
	 * @see test.hotelbeds.temazos.service.ArtistService#delete(test.hotelbeds.temazos.model.Artist)
	 */
	@Override
	public void delete(Long id) {
		artistRepository.delete(id);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see test.hotelbeds.temazos.service.ArtistService#findAll()
	 */
	@Override
	public Iterable<Artist> findAll() {
		return artistRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see test.hotelbeds.temazos.service.ArtistService#findOne(java.lang.Long)
	 */
	@Override
	public Artist findOne(Long id) {
		return artistRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see test.hotelbeds.temazos.service.ArtistService#save(test.hotelbeds.temazos.model.Artist)
	 */
	@Override
	public Artist save(Artist artist) {
		return artistRepository.save(artist);
	}

}
