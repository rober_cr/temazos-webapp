package test.hotelbeds.temazos.service;

import test.hotelbeds.temazos.model.Artist;

/**
 * The Interface ArtistService.
 */
public interface ArtistService {

	/**
	 * Delete.
	 *
	 * @param artist
	 *            the artist
	 */
	public void delete(Long id);

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public Iterable<Artist> findAll();

	/**
	 * Find one.
	 *
	 * @param id
	 *            the id
	 * @return the artist
	 */
	public Artist findOne(Long id);

	/**
	 * Save.
	 *
	 * @param artist
	 *            the artist
	 * @return the artist
	 */
	public Artist save(Artist artist);
}
