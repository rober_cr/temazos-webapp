package test.hotelbeds.temazos.ut.service;

import java.util.ArrayList;
import java.util.List;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import test.hotelbeds.temazos.builder.ArtistBuilder;
import test.hotelbeds.temazos.model.Artist;
import test.hotelbeds.temazos.repository.ArtistRepository;
import test.hotelbeds.temazos.service.ArtistServiceImpl;

/**
 * The Class ArtistServiceImplUT.
 */
@RunWith(JMockit.class)
public class ArtistServiceImplUT {

	/** The artist builder. */
	private ArtistBuilder		artistBuilder;

	/** The artist service. */
	@Tested
	private ArtistServiceImpl	artistService;

	/** The artist repository. */
	@Mocked
	@Injectable
	private ArtistRepository	mockedArtistRepository;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		artistBuilder = ArtistBuilder.artist();
	}

	/**
	 * Test delete.
	 */
	@Test
	public void testDelete() {
		new Expectations() {
			{
				mockedArtistRepository.delete(1l);
			}
		};

		artistService.delete(1l);
	}

	/**
	 * Test find all.
	 */
	@Test
	public void testFindAll() {
		new Expectations() {
			{
				Artist artist = artistBuilder.withId(1l).withName("Chimo Bayo")
						.withPhotoUrl("http://www.kulturaldia.com/wp-web/wp-content/uploads/2015/07/chimo-as%C3%AD-me-gusta-a-m%C3%AD.jpg").build();
				List<Artist> list = new ArrayList<Artist>(1);
				list.add(artist);
				mockedArtistRepository.findAll();
				returns(list);
			}
		};

		List<Artist> artists = (List<Artist>) artistService.findAll();
		Assert.assertNotNull(artists);
		Assert.assertEquals(artists.size(), 1);
		Assert.assertEquals(artists.get(0).getName(), "Chimo Bayo");
	}

	/**
	 * Test find one.
	 */
	@Test
	public void testFindOne() {
		new Expectations() {
			{
				Artist artist = artistBuilder.withId(1l).withName("Chimo Bayo")
						.withPhotoUrl("http://www.kulturaldia.com/wp-web/wp-content/uploads/2015/07/chimo-as%C3%AD-me-gusta-a-m%C3%AD.jpg").build();
				mockedArtistRepository.findOne(1l);
				returns(artist);
			}
		};

		Artist artist = artistService.findOne(1l);
		Assert.assertNotNull(artist);
		Assert.assertEquals(artist.getName(), "Chimo Bayo");
	}

	/**
	 * Test save.
	 */
	@Test
	public void testSave() {
		new Expectations() {
			{
				Artist expectedArtist = artistBuilder.withId(1l).withName("Chimo Bayo")
						.withPhotoUrl("http://www.kulturaldia.com/wp-web/wp-content/uploads/2015/07/chimo-as%C3%AD-me-gusta-a-m%C3%AD.jpg").build();
				mockedArtistRepository.save(expectedArtist);
				returns(expectedArtist);
			}
		};
		Artist artist = artistBuilder.withName("Chimo Bayo").withPhotoUrl("http://www.kulturaldia.com/wp-web/wp-content/uploads/2015/07/chimo-as%C3%AD-me-gusta-a-m%C3%AD.jpg")
				.build();
		artist = artistService.save(artist);
		Assert.assertNotNull(artist);
		Assert.assertEquals(artist.getName(), "Chimo Bayo");
	}
}