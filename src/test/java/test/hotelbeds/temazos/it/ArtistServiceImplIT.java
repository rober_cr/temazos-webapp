package test.hotelbeds.temazos.it;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.hotelbeds.temazos.TemazosWebappApplication;
import test.hotelbeds.temazos.builder.ArtistBuilder;
import test.hotelbeds.temazos.model.Artist;
import test.hotelbeds.temazos.service.ArtistServiceImpl;

/**
 * The Class ArtistServiceImplIT.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { TemazosWebappApplication.class })
@WebIntegrationTest({ "server.port:0" })
@ActiveProfiles("TEST")
public class ArtistServiceImplIT {

	/** The Constant ARTIST_NAME. */
	private static final String	ARTIST_NAME			= "John Scatman";

	/** The Constant ARTIST_PHOTO_URL. */
	private static final String	ARTIST_PHOTO_URL	= "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Scatman5.jpg/245px-Scatman5.jpg";

	/** The artist builder. */
	private ArtistBuilder		artistBuilder;

	/** The artist service. */
	@Autowired
	private ArtistServiceImpl	artistService;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		artistBuilder = ArtistBuilder.artist();
	}

	/**
	 * Test delete.
	 */
	@Test
	public void testDelete() {
		Artist artist = artistBuilder.withName(ARTIST_NAME).withPhotoUrl(ARTIST_PHOTO_URL).build();
		artist = artistService.save(artist);
		artistService.delete(artist.getId());
		Artist recoveredArtist = artistService.findOne(artist.getId());
		Assert.assertNull(recoveredArtist);
	}

	/**
	 * Test find all.
	 */
	@Test
	public void testFindAll() {
		Artist artist = artistBuilder.withName(ARTIST_NAME).withPhotoUrl(ARTIST_PHOTO_URL).build();
		artist = artistService.save(artist);
		List<Artist> artists = (List<Artist>) artistService.findAll();
		Assert.assertNotNull(artists);
		Assert.assertTrue(artists.size() == 1);
	}

	/**
	 * Test find one.
	 */
	@Test
	public void testFindOne() {
		Artist artist = artistBuilder.withName(ARTIST_NAME).withPhotoUrl(ARTIST_PHOTO_URL).build();
		artist = artistService.save(artist);
		Artist recoveredArtist = artistService.findOne(artist.getId());
		Assert.assertNotNull(recoveredArtist);
		Assert.assertTrue(recoveredArtist.getName().equals(ARTIST_NAME));
	}

	/**
	 * Test save.
	 */
	@Test
	public void testSave() {
		Artist artist = artistBuilder.withName(ARTIST_NAME).withPhotoUrl(ARTIST_PHOTO_URL).build();
		artist = artistService.save(artist);
		Artist udpatedArtist = artistService.findOne(artist.getId());
		udpatedArtist.setName("Raphael");
		udpatedArtist = artistService.save(udpatedArtist);
		Assert.assertNotNull(udpatedArtist);
		Assert.assertTrue(udpatedArtist.getName().equals("Raphael"));
	}
}